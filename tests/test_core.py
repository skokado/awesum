import pytest

from awesum import awesum
from awesum.awesum import _as_numeric



class TestAsNumeric:
    def test_as_numeric_for_positive_number(self):
        assert _as_numeric(1) == 1
        assert isinstance(_as_numeric(1), int)
        assert _as_numeric(1.0) == 1.0
        assert isinstance(_as_numeric(1.0), float)


    def test_as_numeric_for_negative_number(self):
        assert _as_numeric(-1) == -1
        assert isinstance(_as_numeric(-1), int)
        assert _as_numeric(-1.0) == -1.0
        assert isinstance(_as_numeric(-1.0), float)


    def test_as_numeric_for_zero(self):
        assert _as_numeric(0) == 0
        assert isinstance(_as_numeric(0), int)
        assert _as_numeric(0.0) == 0
        assert isinstance(_as_numeric(0.0), float)
        

    not_numeric_values = [
        '', 'foo', '123abc',
        'ー１００', '1万'
    ]
    @pytest.mark.filterwarnings('ignore:cannot calculate as numeric')
    @pytest.mark.parametrize('value', not_numeric_values,
                            ids=[f'value={value}' for value in not_numeric_values])
    def test_as_numeric_for_not_numeric_value(self, value):
        assert _as_numeric(value) == 0



class TestAwesum:
    def test_awesum_noargs(self):
        assert awesum() == 0

    awesum_one_args_patters = [
        # args, expected, expected_type
        ([1], 1, int),
        ([1.0], 1.0, float),
        ([0], 0, int),
        ([0.0], 0.0, float),
        ([-1], -1, int),
        ([-1.0], -1.0, float),
    ]
    @pytest.mark.parametrize('args, expected, expected_type', awesum_one_args_patters,
                             ids=[f'args={args}' for args, _, _ in awesum_one_args_patters])
    def test_awesum_one_args(self, args, expected, expected_type):
        result = awesum(*args)
        assert result == expected
        assert isinstance(result, expected_type)

    awesum_two_args_patters = [
        # args, expected, expected_type
        ([1, 0], 1, int),
        ([1, 2], 3, int),
        ([1, -1], 0, int),
        ([0, 0], 0, int),
        ([0, 1], 1, int),
        ([0.0, 1], 1.0, float),
        ([-1, 1], 0, int),
        ([-1, 1.0], 0.0, float),
        ([-1.0, 1.0], 0.0, float),
    ]
    @pytest.mark.parametrize('args, expected, expected_type', awesum_two_args_patters,
                             ids=[f'args={args}' for args, _, _ in awesum_two_args_patters])
    def test_awesum_two_args(self, args, expected, expected_type):
        result = awesum(*args)
        assert result == expected
        assert isinstance(result, expected_type)

