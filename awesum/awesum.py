import warnings


def _as_numeric(value):
    if isinstance(value, (int,
                          float)):
        return value
    if value.isnumeric():
        try:
            return int(value)
        except ValueError:
            pass
        try:
            return float(value)
        except ValueError:
            pass
    warnings.warn('cannot calculate as numeric: %s' % value)
    return 0


def awesum(*args):
    result = 0
    for arg in args:
        value = _as_numeric(arg)
        result += value
    return result
